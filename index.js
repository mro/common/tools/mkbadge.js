#!/usr/bin/env node

const
  { toNumber, forEach, findLast, defaultTo, isEmpty, omitBy } = require('lodash'),
  { makeBadge } = require('badge-maker'),
  cmd = require('commander');

function collectLevel(value, ret) {
  const levels = value.split(',');
  forEach(levels, (value) => {
    value = value.split(':');
    ret.push({ level: toNumber(value[0]), color: value[1] });
  });
  return ret;
}

cmd
.option('-l, --label <value>', 'badge label')
.option('-m, --message <value>', 'badge message')
.option('-c, --color <name>', 'badge message color', 'brightgreen')
.option('-v, --value <value>', 'badge value for automatic threshold')
.option('--level <level:color,level:color>', 'badge automatic threshold', collectLevel, []);
cmd.on('--help', () => {
  console.log('');
  console.log('Example:');
  console.log('  mkbadge -l coverage -v 90 --level 0:red,60:orange,70:yellow,90:brightgreen')
  console.log('  mkbadge -l lint -v 3 --level 0:brightgreen,1:yellow,5:orange,10:red')
});

cmd.parse(process.argv);
const opts = cmd.opts();

opts.message = defaultTo(opts.message, opts.value);
opts.value = toNumber(opts.value);

function getColor(opts) {
  const level = findLast(opts.level, (l) => (l.level <= opts.value));
  return level ? level.color : opts.color;
}

console.log(makeBadge(omitBy({
  label: opts.label,
  message: opts.message,
  color: getColor(opts)
}, isEmpty)));
